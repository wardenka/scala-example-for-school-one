package collection
import scala.io.BufferedSource

/**
 * Считать из файла hdata_stock.csv данные в буфер
 * Преобразовать считанные данные в Tuple5
 * Вывести построчно в консоль считанные полученный результат
 */

object CollectionIssueThree {

  def main(args: Array[String]): Unit = {

    //чтение исходных данных
    val bufferedSource = scala.io.Source.fromResource("hdata_stock.csv")

    //Парсинг стрима и преобразование Tuple5 в Tuple4
    val asdf = getTupleSeq(bufferedSource)
      .map(x => (x._2.toDouble, x._3.toDouble, x._4.toDouble, x._5.toDouble))
    bufferedSource.close

    //Валидация входных значений
    val asdfTupleSize = asdf.map(_.productArity).distinct.head

    //Вычисление средних значений
    val result = pivotListTuple(asdf, asdfTupleSize).map(x => x.map {
      case value: Double => value
    }.sum/x.size)

    result.foreach((bnm) => print(bnm + ",   "))

  }

  //транспонирование "матрицы"
  def pivotListTuple[A <: Product](x: Seq[A], tupleSize: Int): Seq[Seq[Any]] =
    for (
      index <- 0 until tupleSize;
      seq = for (
        element <- x
      ) yield element.productElement(index)
    ) yield seq

  private def getTupleSeq(bufferedSource: BufferedSource) = {
    val list_prices: Seq[(String, String, String, String, String)] = {
      bufferedSource.getLines()
    }.toSeq
      .map(s => s.split(";"))
      .map {
        case Array(f1, f2, f3, f4, f5) => (f1, f2, f3, f4, f5)
        case x => throw new RuntimeException(x.toString).getCause
      }
    list_prices.slice(1, 201)
  }
}
