package collection

import scala.util.Random

/**
 * Заполнить массив заданной длины произвольными положительными числами.
 * Провести анализ списка и найти все числа, которые делятся на два, делятся на три и делятся и на два, и на три.
 * При срабатывании условия выводить соответствующую запись в консоль.
 * Фильтрацию сделать двумя способами:
 * 1) паттерн-матчинг
 * 2) filter
 **/

object CollectionIssueOneV2 {

  def main(args: Array[String]): Unit = {

    val r = Seq.fill(100)(Random.nextInt(100))
    val r2 = r.filter(_ % 6 == 0)
    val r3 = r.filter(_ % 2 == 0)
    val r4 = r.filter(_ % 3 == 0)

    r2.foreach(x => println(x + " Число делится и на два, и на три"))
    r2.diff(r2.diff(r3)).foreach(x => println(x + " Число делится на два"))
    r2.diff(r2.diff(r4)).foreach(x => println(x + " Число делится на три"))

  }

}
